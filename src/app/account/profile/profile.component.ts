import { Component, OnInit, OnDestroy } from '@angular/core';
import { User, PasswordChangeDTO } from 'src/app/shared/models/user.model';
import { Subscription } from 'rxjs';
import { AccountService } from 'src/app/core';
import { UserService } from 'src/app/entities/user-management/user.service';

@Component({
    selector: 'app-profile',
    templateUrl: './profile.component.html',
    styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit, OnDestroy {
    user: User = new User();
    subscriptionManager = new Subscription();
    passwordChangeDTO: PasswordChangeDTO = new PasswordChangeDTO();
    confirmPassword: string;
    donotMatch = false;
    constructor(private accountService: AccountService, private userService: UserService) {}

    ngOnInit() {
        this.getAccount();
    }

    getAccount() {
        this.accountService.identity(true).then(res => {
            this.user = res;
        });
    }

    saveUserDetails() {
        this.subscriptionManager.add(
            this.userService.saveUserBasicDetails(this.user).subscribe(() => {
                this.getAccount();
                this.userService.refreshUser.next(true);
            })
        );
    }

    ngOnDestroy() {
        if (this.subscriptionManager) {
            this.subscriptionManager.unsubscribe();
        }
    }

    changePassword() {
        this.donotMatch = false;
        if (this.confirmPassword && this.passwordChangeDTO.newPassword && this.confirmPassword === this.passwordChangeDTO.newPassword) {
            if (this.passwordChangeDTO.currentPassword) {
                this.subscriptionManager.add(this.userService.changePassword(this.passwordChangeDTO).subscribe());
            }
        } else {
            this.donotMatch = true;
        }
    }
}
