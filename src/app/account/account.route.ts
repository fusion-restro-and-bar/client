import { LoginComponent } from './login/login.component';
import { Routes } from '@angular/router';
import { ResetPasswordInitComponent } from './reset-password-init/reset-password-init.component';
import { ProfileComponent } from './profile/profile.component';

export const accountRoutes: Routes = [
    {
        path: 'login',
        component: LoginComponent,
        data: {
            pageTitle: 'Fusion Restro & Bar - Login',
            dashboardTitle: 'Login'
        }
    },
    {
        path: '',
        redirectTo: 'login',
        pathMatch: 'full'
    },
    {
        path: 'reset-password/init',
        component: ResetPasswordInitComponent,
        data: {
            pageTitle: 'Fusion Restro & Bar - Reset Password',
            dashboardTitle: 'Reset Password'
        }
    }
];
