import { NgModule } from '@angular/core';
import { LoginComponent } from './login/login.component';
import { ResetPasswordInitComponent } from './reset-password-init/reset-password-init.component';
import { ResetPasswordFinishComponent } from './reset-password-finish/reset-password-finish.component';
import { SharedModule } from '../shared/shared.module';
import { RouterModule } from '@angular/router';
import { accountRoutes } from './account.route';

@NgModule({
  declarations: [LoginComponent, ResetPasswordInitComponent, ResetPasswordFinishComponent],
  imports: [
    SharedModule,
    RouterModule.forChild(accountRoutes)
  ]
})
export class AccountModule { }
