import { Component, OnInit, OnDestroy } from '@angular/core';
import { AlertService } from 'src/app/shared/utils/alert.service';
import { Subscription } from 'rxjs';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'app-entities-layout',
    templateUrl: './entities-layout.component.html',
    styleUrls: ['./entities-layout.component.scss']
})
export class EntitiesLayoutComponent implements OnInit, OnDestroy {
    subscription = new Subscription();
    constructor(private alertService: AlertService, private spinner: NgxSpinnerService) {}

    ngOnInit() {
        this.subscription.add(
            this.alertService.showLoader.subscribe(res => {
                if (res) {
                    this.spinner.show();
                } else {
                    this.spinner.hide();
                }
            })
        );
    }

    ngOnDestroy() {
        if (this.subscription) {
            this.subscription.unsubscribe();
        }
    }
}
