import { Routes } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';
import { UserManagementComponent } from './user-management/user-management.component';
import { AddUserComponent } from './user-management/add-user/add-user.component';
import { ProfileComponent } from '../account/profile/profile.component';
import { OrderComponent } from './order/order.component';
import { MenuListComponent } from './menu-list/menu-list.component';

export const entityRoutes: Routes = [
    {
        path: '',
        redirectTo: 'dashboard',
        pathMatch: 'full'
    },
    {
        path: 'dashboard',
        component: DashboardComponent,
        data: {
            pageTitle: 'Fusion Restro & Bar - Dashboard',
            dashboardTitle: 'Dashboard'
        }
    },
    {
        path: 'users',
        component: UserManagementComponent,
        data: {
            title: 'Fusion Restro & Bar - Users',
            dashboardTitle: 'Users'
        }
    },
    {
        path: 'add-user',
        component: AddUserComponent,
        data: {
            title: 'Fusion Restro & Bar - Users',
            dashboardTitle: 'Users'
        }
    },
    {
        path: 'profile',
        component: ProfileComponent,
        data: {
            pageTitle: 'Fusion Restro & Bar - User Profile',
            dashboardTitle: 'User Profile'

        }
    },
    {
        path: 'orders',
        component: OrderComponent,
        data: {
            pageTitle: 'Fusion Restro & Bar - Orders',
            dashboardTitle: 'Orders'

        }
    },
    {
        path: 'menu',
        component: MenuListComponent,
        data: {
            pageTitle: 'Fusion Restro & Bar - Menu',
            dashboardTitle: 'Menu'

        }
    }
];
