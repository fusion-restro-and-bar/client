import { Component, OnInit, OnDestroy } from '@angular/core';
import { KNOB_OPTIONS } from 'src/app/shared/constants/app.constants';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit, OnDestroy {
  totalEmployees = 123042;
  leavesTakenOptions = { ...KNOB_OPTIONS, barColor: '#0781ba' };
  leaveAvailableValue = 0;
  leavesAvailableOptions = { ...KNOB_OPTIONS, barColor: '#26c977' };
  advanceValue = 0;
  totalAdvanceTaken = 23000;
  advancesOptions = { ...KNOB_OPTIONS, barColor: '#0781ba' };
  oldSchoolEmployees = 0;
  lePriveEmployees = 0;
  privateBanquetEmployees = 0;
  subscriptionManager = new Subscription();
  constructor() {}

  ngOnInit() {}

  ngOnDestroy() {
    if (this.subscriptionManager) {
      this.subscriptionManager.unsubscribe();
    }
  }
}
