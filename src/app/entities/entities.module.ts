import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';
import { DashboardComponent } from './dashboard/dashboard.component';
import { RouterModule } from '@angular/router';
import { entityRoutes } from './entities.route';
import { UserManagementComponent } from './user-management/user-management.component';
import { AddUserComponent } from './user-management/add-user/add-user.component';
import { ProfileComponent } from '../account/profile/profile.component';
import { OrderComponent } from './order/order.component';
import { TablesComponent } from './tables/tables.component';
import { MenuComponent } from './menu/menu.component';
import { StockManagementComponent } from './stock-management/stock-management.component';
import { ReportsComponent } from './reports/reports.component';
import { MenuListComponent } from './menu-list/menu-list.component';

@NgModule({
    declarations: [
        DashboardComponent,
        UserManagementComponent,
        AddUserComponent,
        ProfileComponent,
        OrderComponent,
        TablesComponent,
        MenuComponent,
        StockManagementComponent,
        ReportsComponent,
        MenuListComponent
    ],
    imports: [SharedModule, RouterModule.forChild(entityRoutes)],
    exports: []
})
export class EntitiesModule {}
