import {
  Component,
  OnInit,
  ViewChild,
  TemplateRef,
  OnDestroy,
  ChangeDetectorRef
} from '@angular/core';
import { TabsetComponent, BsModalRef, BsModalService } from 'ngx-bootstrap';
import * as $ from 'jquery';
import { Subscription } from 'rxjs';
import { MenuService } from './menu.service';
import { AlertService } from 'src/app/shared/utils/alert.service';
import { MenuCategory, MenuItem } from 'src/app/shared/models/common.model';

@Component({
  selector: 'app-menu-list',
  templateUrl: './menu-list.component.html',
  styleUrls: ['./menu-list.component.scss']
})
export class MenuListComponent implements OnInit, OnDestroy {
  @ViewChild('staticTabs') staticTabs: TabsetComponent;
  modalRef: BsModalRef;
  @ViewChild('template') template: any;
  categoryName: string;
  subscription = new Subscription();
  categories: MenuCategory[] = [];
  menuItem: MenuItem = new MenuItem();
  selectedTab: MenuCategory = new MenuCategory();
  constructor(
    private modalService: BsModalService,
    private menuService: MenuService,
    private cdf: ChangeDetectorRef,
    private alertService: AlertService
  ) {}

  ngOnInit() {
    this.getAllCategories();
    $('h3 span:first-child').after('<span class="dots"> </span>');
  }

  openAddCategoryModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template);
  }

  addCategory() {
    this.subscription.add(
      this.menuService.addCategory(this.categoryName).subscribe(res => {
        this.alertService.successToast('Menu Category is added successfully');
        this.getAllCategories();
        this.categoryName = '';
        this.modalRef.hide();
      })
    );
  }

  deleteCategory() {
    this.subscription.add(
      this.menuService.deleteCategory(this.selectedTab.id).subscribe(() => {
        this.alertService.successToast('Menu Category is deleted successfully');
        this.selectedTab = new MenuCategory();
        this.getAllCategories();
      })
    );
  }

  getAllCategories() {
    this.subscription.add(
      this.menuService.getAllCategories().subscribe(res => {
        this.categories = res;
        this.cdf.detectChanges()
        if (!this.selectedTab || !this.selectedTab.id) {
          this.selectedTab =
            this.categories.length > 0
              ? this.categories[0]
              : new MenuCategory();
          if (this.categories.length > 0) {
            this.categories[0].active = true;
          }
        } else {
          this.categories.forEach(category => {
            if (this.selectedTab && this.selectedTab.id === category.id) {
              category.active = true;
            }
          });
        }
      })
    );
  }

  selectTab(tab) {
    tab.active = true;
    this.selectedTab = tab;
  }

  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

  addMenuItem() {
    this.menuItem.menuCategory = this.selectedTab;
    this.subscription.add(
      this.menuService.addMenuItem(this.menuItem).subscribe(res => {
        this.alertService.successToast('New Menu Item is added successfully');
        this.modalRef.hide();
        this.menuItem = new MenuItem();
        this.getAllCategories();
      })
    );
  }

  deleteMenuItem(id) {
    this.subscription.add(
      this.menuService.deleteMenuItem(id).subscribe(res => {
        this.alertService.successToast('Menu Item is deleted successfully');
        this.getAllCategories();
      })
    );
  }
}
