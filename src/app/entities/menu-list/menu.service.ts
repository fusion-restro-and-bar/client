import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { API_URL } from 'src/app/shared/constants/url.constants';
import { MenuItem } from 'src/app/shared/models/common.model';

@Injectable({
  providedIn: 'root'
})
export class MenuService {

  constructor(private http: HttpClient) { }

  addCategory(categoryName): Observable<any> {
    return this.http.post(API_URL.CATEGORY, {categoryName});
  }

  deleteCategory(id): Observable<any> {
    return this.http.delete(`${API_URL.CATEGORY}/${id}`);
  }

  getAllCategories(): Observable<any> {
    return this.http.get(API_URL.CATEGORY);
  }

  addMenuItem(menuItem: MenuItem): Observable<any> {
    return this.http.post(API_URL.MENU_ITEM, menuItem);
  }

  deleteMenuItem(id?: any): Observable<any> {
    return this.http.delete(`${API_URL.MENU_ITEM}/${id}`);
  }
}
