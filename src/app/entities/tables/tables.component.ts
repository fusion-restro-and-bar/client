import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-tables',
  templateUrl: './tables.component.html',
  styleUrls: ['./tables.component.scss']
})
export class TablesComponent implements OnInit {
  @Input() tableNumber: number;
  @Input() tableCost: number;
  @Input() selectedTable = false;
  constructor() { }

  ngOnInit() {
  }

}
