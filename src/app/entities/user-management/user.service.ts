import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { API_URL } from 'src/app/shared/constants/url.constants';
import { PasswordChangeDTO } from 'src/app/shared/models/user.model';

@Injectable({
    providedIn: 'root'
})
export class UserService {
    public refreshUser = new Subject<boolean>();
    constructor(private http: HttpClient) {}

    getAllUsers(): Observable<any> {
        return this.http.get(API_URL.USERS);
    }

    editUser(user): Observable<any> {
        return this.http.put(API_URL.USERS, user);
    }

    deleteUser(id): Observable<any> {
        return this.http.delete(`${API_URL.USERS}/${id}`);
    }

    getUserById(login): Observable<any> {
        return this.http.get(`${API_URL.USERS}/${login}`);
    }

    saveUser(user): Observable<any> {
        return this.http.post(API_URL.USERS, user);
    }

    saveUserBasicDetails(user): Observable<any> {
        return this.http.post(API_URL.ACCOUNT, user);
    }

    changePassword(passwordChangeDTO: PasswordChangeDTO): Observable<any> {
        return this.http.post(API_URL.CHANGE_PASSWORD, passwordChangeDTO);
    }
}
