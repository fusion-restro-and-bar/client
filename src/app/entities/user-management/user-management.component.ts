import { Component, OnInit, ChangeDetectorRef, OnDestroy, TemplateRef, ViewChild } from '@angular/core';
import * as $ from 'jquery';
import { UserService } from './user.service';
import { User } from 'src/app/shared/models/user.model';
import { Subscription } from 'rxjs';
import { AlertService } from 'src/app/shared/utils/alert.service';
import { USERS_TO_EXCLUDE, ROLE_ADMIN } from 'src/app/shared/constants/app.constants';
import { AccountService } from 'src/app/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';

@Component({
    selector: 'app-user-management',
    templateUrl: './user-management.component.html',
    styleUrls: ['./user-management.component.scss']
})
export class UserManagementComponent implements OnInit, OnDestroy {
    dataTable: any;
    users: User[] = [];
    subscriptionManager = new Subscription();
    showEdit = false;
    modalRef: BsModalRef;
    @ViewChild('template') template: any;

    constructor(
        private cdf: ChangeDetectorRef,
        private userService: UserService,
        private alertService: AlertService,
        private accountService: AccountService,
        private modalService: BsModalService
    ) {}

    ngOnInit() {
        this.getAllUsers();
        this.accountService.hasAuthority(ROLE_ADMIN).then(res => {
            this.showEdit = res;
        });
    }

    getAllUsers() {
        this.subscriptionManager.add(this.userService.getAllUsers().subscribe(res => this.onSuccess(res)));
    }

    onSuccess(data) {
        const table: any = $('#user-mgmt');
        table.DataTable().destroy();
        this.users = data.filter(user => !USERS_TO_EXCLUDE.includes(user.email));
        this.cdf.detectChanges();
        this.dataTable = table.DataTable();
    }

    ngOnDestroy() {
        if (this.subscriptionManager) {
            this.subscriptionManager.unsubscribe();
        }
    }

    getRole(authorities) {
        let roles = [];
        roles = authorities.map((authority: string) => {
            return authority.includes('_') ? ` ${authority.split('_')[1]}` : authority;
        });
        return roles;
    }

    deleteUser(login?: string) {
        this.subscriptionManager.add(this.userService.deleteUser(login).subscribe(() => this.getAllUsers()));
    }

    async removeUser(email) {
        if (await this.alertService.confirmAlert('Are you sure you want to delete user? You wont be able to revert this back.', '')) {
            this.deleteUser(email);
        }
    }

    openAddTableModal(template: TemplateRef<any>) {
        this.modalRef = this.modalService.show(template);
    }
}
