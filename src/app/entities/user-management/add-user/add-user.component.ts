import { Component, OnInit, OnDestroy } from '@angular/core';
import { Location } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { User } from 'src/app/shared/models/user.model';
import { Subscription } from 'rxjs';
import { UserService } from '../user.service';
import { AUTHORITIES } from 'src/app/shared/constants/app.constants';
import { AlertService } from 'src/app/shared/utils/alert.service';

@Component({
    selector: 'app-add-user',
    templateUrl: './add-user.component.html',
    styleUrls: ['./add-user.component.scss']
})
export class AddUserComponent implements OnInit, OnDestroy {
    user: User = new User();
    userId: any;
    subscriptionManager = new Subscription();
    authorities = AUTHORITIES;
    constructor(
        private location: Location,
        private activatedRoute: ActivatedRoute,
        private userService: UserService,
        private alertService: AlertService
    ) {}

    ngOnInit() {
        this.userId = this.activatedRoute.snapshot.paramMap.get('id');
        if (this.userId) {
            this.getUser(this.userId);
        }
    }

    cancel() {
        this.location.back();
    }

    getUser(userId) {
        this.subscriptionManager.add(
            this.userService.getUserById(userId).subscribe(res => {
                this.user = res;
            })
        );
    }

    saveUser() {
        if (this.userId) {
            this.subscriptionManager.add(
                this.userService.editUser(this.user).subscribe(res => {
                    this.alertService.success('Success', 'User details are modified successfully.');
                    this.cancel();
                })
            );
        } else {
            this.user.login = this.user.email;
            this.subscriptionManager.add(
                this.userService.saveUser(this.user).subscribe(res => {
                    this.alertService.success('Success', 'New User is created successfully.');
                    this.cancel();
                })
            );
        }
    }

    ngOnDestroy() {
        if (this.subscriptionManager) {
            this.subscriptionManager.unsubscribe();
        }
    }
}
