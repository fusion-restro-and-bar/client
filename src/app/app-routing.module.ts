import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainLayoutComponent } from './shared/components/main-layout/main-layout.component';
import { EntitiesLayoutComponent } from './entities/entities-layout/entities-layout.component';

const routes: Routes = [
    { path: '', component: MainLayoutComponent, loadChildren: './account/account.module#AccountModule'},
    { path: 'app', loadChildren: './entities/entities.module#EntitiesModule', component: EntitiesLayoutComponent }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {}
