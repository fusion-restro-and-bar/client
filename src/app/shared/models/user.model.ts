export class User {
    public id?: number;
    public login?: string;
    public firstName?: string;
    public lastName?: string;
    public email?: string;
    public createdDate?: any;
    public authorities?: any;
    public password?: string;
}

export class PasswordChangeDTO {
    public currentPassword?: string;
    public newPassword?: string;
}
