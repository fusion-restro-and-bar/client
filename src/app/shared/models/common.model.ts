export class MenuCategory {
    public id?: number;
    public categoryName?: string;
    public menuItemList?: MenuItem[] = [];
    public active?: boolean;
}

export class MenuItem {
    public id?: number;
    public itemName?: string;
    public price?: number;
    public menuCategory?: MenuCategory = new MenuCategory();
}
