import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { LocalStorageService, SessionStorageService } from 'ngx-webstorage';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpResponse } from '@angular/common/http';
import { SERVER_API_URL } from '../constants/app.constants';
import { environment } from 'src/environments/environment';
import { AlertService } from '../utils/alert.service';
import { tap } from 'rxjs/operators';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
    constructor(
        private localStorage: LocalStorageService,
        private sessionStorage: SessionStorageService,
        private alertService: AlertService
    ) {}

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        this.alertService.showLoader.next(true);
        if (!request || !request.url || (/^http/.test(request.url) && !(SERVER_API_URL && request.url.startsWith(SERVER_API_URL)))) {
            return next.handle(request);
        }

        const token = this.localStorage.retrieve('authenticationToken') || this.sessionStorage.retrieve('authenticationToken');
        if (!!token) {
            request = request.clone({
                setHeaders: {
                    Authorization: 'Bearer ' + token
                }
            });
        }
        request = request.clone({ url: environment.SERVER_API_URL + request.url });
        return next.handle(request).pipe(
            tap(event => {
                if (event instanceof HttpResponse) {
                    this.requestSuccess(event);
                }
            })
        );
    }

    private requestSuccess(res: HttpResponse<any>) {
        this.alertService.showLoader.next(false);
        return res;
    }
}
