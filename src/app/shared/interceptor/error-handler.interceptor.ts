import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpErrorResponse, HttpHandler, HttpEvent } from '@angular/common/http';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { AlertService } from '../utils/alert.service';

@Injectable()
export class ErrorHandlerInterceptor implements HttpInterceptor {
    constructor(private alertService: AlertService) {}

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        return next.handle(request).pipe(
            tap(
                (event: HttpEvent<any>) => {},
                (err: any) => {
                    if (err instanceof HttpErrorResponse) {
                        this.alertService.showLoader.next(false);
                        if (err.status === 401) {
                            this.alertService.error('Login Error', 'Invalid email or password');
                        } else if (err.status === 400 && err.error.status === 400) {
                            this.alertService.error('Error', err.error.title);
                        } else if (err.status === 500 || err.status === 400) {
                            this.alertService.error('Server problem', 'There was a problem. Please try again or contact administrator');
                        } else if (err.status === 0) {
                            this.alertService.error(
                                'Server Unavailable',
                                'Server is not available at the moment. Please contact administrator.'
                            );
                        }
                    }
                }
            )
        );
    }
}
