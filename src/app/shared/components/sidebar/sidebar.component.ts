import { Component, OnInit, OnDestroy } from '@angular/core';
import { LoginService, AccountService, User } from 'src/app/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { UserService } from 'src/app/entities/user-management/user.service';

@Component({
    selector: 'app-sidebar',
    templateUrl: './sidebar.component.html',
    styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit, OnDestroy {
    user: User;
    subscriptionManager = new Subscription();
    constructor(
        private loginService: LoginService,
        private router: Router,
        private accountService: AccountService,
        private userService: UserService
    ) {}

    ngOnInit() {
        this.getAccount();
        this.subscriptionManager.add(
            this.userService.refreshUser.subscribe(res => {
                if (res) {
                    this.getAccount();
                }
            })
        );
    }

    logout() {
        this.loginService.logout();
        this.router.navigate(['']);
    }

    getAccount() {
        this.accountService.identity(true).then(res => {
            this.user = res;
        });
    }

    ngOnDestroy() {
        if (this.subscriptionManager) {
            this.subscriptionManager.unsubscribe();
        }
    }
}
