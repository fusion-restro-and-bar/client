import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { Router, ActivatedRouteSnapshot, NavigationEnd, NavigationError } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

    title: string;
    dashboardTitle: string;

    constructor(private titleService: Title, private router: Router) {}

    private getPageTitle(routeSnapshot: ActivatedRouteSnapshot) {
        let title: string = routeSnapshot.data && routeSnapshot.data.title ? routeSnapshot.data.title : 'Fusion Restro & Bar';
        this.dashboardTitle = routeSnapshot.data && routeSnapshot.data.dashboardTitle ? routeSnapshot.data.dashboardTitle : 'Dashboard';
        if (routeSnapshot.firstChild) {
            title = this.getPageTitle(routeSnapshot.firstChild) || title;
        }
        return title;
    }

    ngOnInit() {
        this.title = this.getPageTitle(this.router.routerState.snapshot.root);
        if (this.title) {
            this.titleService.setTitle(this.title);
        }
        this.router.events.subscribe(event => {
            if (event instanceof NavigationEnd) {
                this.title = this.getPageTitle(this.router.routerState.snapshot.root);
                this.titleService.setTitle(this.title);
            }
            if (event instanceof NavigationError && event.error.status === 404) {
                this.router.navigate(['/404']);
            }
        });
    }

}
