import { Injectable } from '@angular/core';
import Swal from 'sweetalert2';
import { Subject } from 'rxjs';
import { ToastrService } from 'ngx-toastr';

@Injectable({ providedIn: 'root' })
export class AlertService {
    public showLoader = new Subject<boolean>();
    constructor(private toastr: ToastrService) {}

    async success(title, msg) {
        return await Swal({
            title,
            text: msg,
            type: 'success'
        }).then(result => {
            if (result.value) {
                return true;
            } else {
                return false;
            }
        });
    }

    async error(title, msg) {
        return await Swal({
            title,
            text: msg,
            type: 'error'
        }).then(result => {
            if (result.value) {
                return true;
            } else {
                return false;
            }
        });
    }

    async warn(title, msg) {
        return await Swal({
            title,
            text: msg,
            type: 'warning'
        }).then(result => {
            if (result.value) {
                return true;
            } else {
                return false;
            }
        });
    }

    async info(title, msg) {
        return await Swal({
            title,
            text: msg,
            type: 'info'
        }).then(result => {
            if (result.value) {
                return true;
            } else {
                return false;
            }
        });
    }

    async confirmAlert(title, message) {
        return await Swal({
            title: title ? title : '',
            text: message ? message : '',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes',
            cancelButtonText: 'No'
        }).then(result => {
            if (result.value) {
                return true;
            } else {
                return false;
            }
        });
    }

    successToast(message) {
        this.toastr.success('', message);
    }

    errorToast(message) {
        this.toastr.error('', message);
    }

    infoToast(message) {
        this.toastr.info('', message);
    }
}
