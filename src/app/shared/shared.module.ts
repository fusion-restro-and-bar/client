import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MainLayoutComponent } from './components/main-layout/main-layout.component';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { EntitiesLayoutComponent } from '../entities/entities-layout/entities-layout.component';
import { HeaderComponent } from './components/header/header.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import 'datatables.net';
import 'datatables.net-bs4';
import 'datatables.net-responsive-bs4';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { TimepickerModule } from 'ngx-bootstrap/timepicker';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { KnobModule } from '@xmlking/ngx-knob';
import * as d3 from 'd3';
import 'd3-selection-multi';
import { ChartsModule } from 'ng2-charts';
import { ModalModule } from 'ngx-bootstrap/modal';
import { HasAnyAuthorityDirective } from './directives/has-authority.directive';
import { HttpClientModule } from '@angular/common/http';
import { BarRatingModule } from 'ngx-bar-rating';
import { NgxSpinnerModule } from 'ngx-spinner';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { ToastrModule } from 'ngx-toastr';

@NgModule({
    declarations: [MainLayoutComponent, EntitiesLayoutComponent, HeaderComponent, SidebarComponent, HasAnyAuthorityDirective],
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        RouterModule,
        BsDatepickerModule.forRoot(),
        TimepickerModule.forRoot(),
        TooltipModule.forRoot(),
        KnobModule,
        ChartsModule,
        ModalModule.forRoot(),
        HttpClientModule,
        BarRatingModule,
        NgxSpinnerModule,
        TabsModule.forRoot(),
        ToastrModule.forRoot({
            timeOut: 5000,
            positionClass: 'toast-bottom-left',
            preventDuplicates: true,
            closeButton: true
          })
    ],
    exports: [
        CommonModule,
        RouterModule,
        FormsModule,
        ReactiveFormsModule,
        EntitiesLayoutComponent,
        HeaderComponent,
        SidebarComponent,
        BsDatepickerModule,
        TimepickerModule,
        TooltipModule,
        KnobModule,
        ChartsModule,
        ModalModule,
        HasAnyAuthorityDirective,
        HttpClientModule,
        BarRatingModule,
        NgxSpinnerModule,
        TabsModule,
        ToastrModule
    ]
})
export class SharedModule {}
