import { environment } from 'src/environments/environment';

export const SERVER_API_URL = environment.SERVER_API_URL;
export const KNOB_OPTIONS = {
    readOnly: true,
    size: 80,
    unit: '%',
    textColor: '#000000',
    fontSize: '15',
    fontWeigth: '700',
    fontFamily: 'Roboto',
    valueformat: 'percent',
    value: 0,
    max: 300,
    trackWidth: 6,
    barWidth: 7,
    trackColor: '#D8D8D8',
    barColor: '#FF6F17',
    subText: {
        enabled: false
    }
};
export const AUTHORITIES = ['ROLE_ADMIN', 'ROLE_USER'];
export const USERS_TO_EXCLUDE = [];
export const ROLE_ADMIN = 'ROLE_ADMIN';
export const ITEM_QUANTITIES = ['1', '30', '60'];
