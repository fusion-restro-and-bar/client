export const API_URL = {
    USERS: '/users',
    ACCOUNT: '/account',
    CHANGE_PASSWORD: '/account/change-password',
    MENU_ITEM: '/menu-items',
    CATEGORY: '/menu-categories'
}
